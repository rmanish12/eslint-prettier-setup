# ESLint and Prettier Configuration

#### Install dependencies

```
npm i -D eslint prettier eslint-plugin-prettier eslint-config-prettier eslint-plugin-node eslint-config-node

npx install-peerdeps --dev eslint-config-airbnb
```

#### For reference use these websites
https://www.npmjs.com/package/eslint-config-airbnb - Airbnb npm module
https://prettier.io/ - Prettier
https://eslint.org/docs/rules/ - Eslint rules

#### VSCode Extensions to install
1. ESLint
2. Prettier

#### Command to run linting for all the files
```npm run lint```